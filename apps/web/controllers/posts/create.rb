module Web::Controllers::Posts
  class Create
    include Web::Action

    def call(params)
      PostRepository.new.create(params[:post])

      redirect_to '/posts'
    end
  end
end
