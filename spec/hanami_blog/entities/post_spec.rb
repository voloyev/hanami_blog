require_relative '../../spec_helper'

describe Post do
  it 'creates post' do
    post = Post.new(title: 'First post', body: 'First post body')
    post.title.must_equal('First post')
    post.body.must_equal('First post body')
  end
end
